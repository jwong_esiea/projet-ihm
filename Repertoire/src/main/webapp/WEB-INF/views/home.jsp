<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Repertoire Project Vedovato - Wong</title>
	<!-- Bootstrap -->
    <link href="resources/css/bootstrap.css" rel="stylesheet" media="screen">
</head>
<body>

<script src="resources/js/jquery.js"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/fonction.js"></script>
    <div class="container">
    	<div class="hero-unit">
    		<h1>Bienvenue !</h1>
    		 <p>Ici vous pouvez gerer vos contacts ! Cliquer sur le bouton ajouter contact pour ajouter un contact</p>
    		 <p><a onclick="add_contact()" class="btn btn-primary btn-large"><i class="icon-user icon-white"></i>  Ajouter un contact</a></p>
			
		</div>
	</div>
<!-- modal Part -->	
	<div class="modal hide fade" id="add_contact_modal">
			<div class="modal-header ">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Ajouter un contact</h3>
			</div>
			<div class="modal-body">
			
			<form> 
				<fieldset style="text-align:center;">
					<legend>Ajout de contact</legend>
					<label></label>
					
					<span class="help-block"></span>
					
					<div class="input-prepend">
						<span class="add-on">Nom </span>
						<input id="nom_contact" type="text" placeholder="indiquer le nom de votre contact...">
					</div>
					
					<span class="help-block"></span>
					
					<div class="input-prepend">
					<span class="add-on">Pr�nom</span>					
					<input id="prenom_contact" type="text" placeholder="indiquer le pr�nom de votre contact...">
					</div>
					
					<span class="help-block"></span>
					
					<div class="input-prepend">
						<span class="add-on">@</span>
						<input id="email_contact" type="text" placeholder="indiquer votre e-mail...">
					</div>
					
					<span class="help-block"></span>
					
					<div class="input-prepend">
						<span class="add-on">Date de naissance</span>	
					<input id="birthday_contact" type="text" placeholder="indiquer la date de naissance...">
					</div>
					
					<span class="help-block"></span>
					
					<h4>Adresse</h4>
										
					<div class="input-prepend">
						<span class="add-on">Num�ro</span>
						<input type="text" id="numero_rue_contact" placeholder="indiquer le N� de la rue ...">
					</div>	
					
					<span class="help-block"></span>
					
					<div class="input-prepend">
						<span class="add-on">Nom de la rue/Avenue</span>
						<input type="text" id="nom_rue_contact" placeholder="indiquer la rue/Avenue ...">
					</div>	
					
					<span class="help-block"></span>
					
					<div class="input-prepend">
						<span class="add-on">Code Postale</span>
						<input type="text" id="CP_contact" placeholder="indiquer le Code postal">
					</div>	
					
					<span class="help-block"></span>
					
					<div class="input-prepend">
						<span class="add-on">Nom de la Ville</span>
						<input type="text" id="Ville_contact" placeholder="indiquer la Ville ...">
					</div>	
					
					
					
					<span class="help-block"></span>
					
				</fieldset>
			</form>
			</div>
		<div class="modal-footer">
			<a class="btn btn-info" onclick="push_new_contact()"> Ajouter contact </a>
			<a class="btn btn-info" onclick="push_clean_form()"> Nettoyer formulaire </a>
			<a class="btn btn-danger" onclick="push_back()" >Retour</a>
			
		</div>
	</div>
	
	<!-- Contact part -->
	
	
	
</body>
</html>
