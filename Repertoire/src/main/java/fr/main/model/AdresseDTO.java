package fr.main.model;

import java.util.ArrayList;
import java.util.List;

public class AdresseDTO {
	private List<Adresse> adresseList;
	private static AdresseDTO adresseDTO;
	private final static int OPTION_SEARCH_IDADRESSE = 0;
	private final static int OPTION_SEARCH_RUE = 1;
	private final static int OPTION_SEARCH_CODE_POSTAL = 2;
	private final static int OPTION_SEARCH_CODE_VILLE = 3;

	public AdresseDTO() {
		adresseList = new ArrayList<Adresse>();
	}

	public boolean addAdresse(Adresse newAdresse) {
		newAdresse.setIdAdresse(adresseList.size() + 1);
		return adresseList.add(newAdresse);
	}

	public boolean deleteAdresse(Adresse deleteAdresse) {
		return adresseList.remove(deleteAdresse);
	}

	public Adresse modifyAdresse(Adresse modifyAdresse, int index) {
		return adresseList.set(index, modifyAdresse);
	}

	public List<Adresse> findAnAdresse(int option, String component) {
		List<Adresse> adresseFounded = new ArrayList<Adresse>();

		for (Adresse adresse : adresseList) {

			switch (option) {
			case OPTION_SEARCH_IDADRESSE:
				if (adresse.getIdAdresse() == adresse.getIdAdresse())
					adresseFounded.add(adresse);
				break;
			case OPTION_SEARCH_RUE:
				if (adresse.getRue().equals(adresse.getRue()))
					adresseFounded.add(adresse);
				break;
			case OPTION_SEARCH_CODE_VILLE:
				if (adresse.getVille().equals(adresse.getVille()))
					adresseFounded.add(adresse);
				break;
			case OPTION_SEARCH_CODE_POSTAL:
				if (adresse.getCodePostal().equals(adresse.getCodePostal()))
					adresseFounded.add(adresse);
				break;
			default:
				break;
			}
		}
		return adresseFounded;
	}

	public static AdresseDTO getAdresseDTO() {
		if (adresseDTO == null) {
			adresseDTO = new AdresseDTO();
		}
		return adresseDTO;
	}

}