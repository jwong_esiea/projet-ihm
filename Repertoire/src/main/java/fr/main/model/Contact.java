package fr.main.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Contact {

	private int idContact;
	private String nom;
	private String prenom;
	private String email;
	private Date dateNaissance;
	private List<Adresse> adresses;
	
	public Contact() {
		super();
	}
	public Contact(int idContact,String nom, String prenom, String email, Date dateNaissance) {
		this.idContact = idContact;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.dateNaissance = dateNaissance;
		this.adresses = new ArrayList<Adresse>();
	}

	public int getIdContact() {
		return idContact;
	}

	public void setIdContact(int idContact) {
		this.idContact = idContact;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public List<Adresse> getAdresses() {
		return adresses;
	}

	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}

}
