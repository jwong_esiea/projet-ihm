package fr.main.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContactDTO {
	private List<Contact> contactList;
	private static ContactDTO contactDTO;
	private final static int OPTION_FIND_BY_IDCONTACT = 6;
	private final static int OPTION_FIND_BY_FIRSTNAME = 1;
	private final static int OPTION_FIND_BY_NAME = 2;
	private final static int OPTION_FIND_BY_ADRESS = 3;
	private final static int OPTION_FIND_BY_DATEOFBIRTH = 4;
	private final static int OPTION_FIND_BY_EMAIL = 5;

	public ContactDTO() {
		super();
		this.contactList = new ArrayList<Contact>();
	}

	public ContactDTO(List<Contact> contactList) {
		super();
		this.contactList = contactList;
	}

	/**
	 * Return an contact list
	 * 
	 * @param element
	 *            : Element we want to find
	 * @param option
	 *            : in which attribute we want to find the element
	 * 
	 * @return
	 */
	private List<Contact> findContactByOneElementOption(String element,
			int option) {
		List<Contact> contactList = new ArrayList<Contact>();
		if (!element.isEmpty() && option != 0) {
			for (Contact contact : this.contactList) {
				switch (option) {
				case OPTION_FIND_BY_IDCONTACT:
					if (contact.getIdContact() == Integer.parseInt(element))
						contactList.add(contact);
				case OPTION_FIND_BY_FIRSTNAME:
					if (contact.getNom().equals(element))
						contactList.add(contact);
					break;
				case OPTION_FIND_BY_NAME:
					if (contact.getPrenom().equals(element))
						contactList.add(contact);
					break;
				case OPTION_FIND_BY_ADRESS:
					if (contact.getAdresses().equals(element))
						contactList.add(contact);
					break;
				case OPTION_FIND_BY_DATEOFBIRTH:
					if (contact.getDateNaissance().equals(element))
						contactList.add(contact);
					break;
				case OPTION_FIND_BY_EMAIL:
					if (contact.getEmail().equals(element))
						contactList.add(contact);
					break;
				default:

					break;
				}
			}
		}
		return contactList;

	}

	/**
	 * Find the contact By the Id
	 * 
	 * @param name
	 * @return
	 */
	public Contact findContactByCid(int Cid) {
		if (this.findContactByOneElementOption(String.valueOf(Cid),
				OPTION_FIND_BY_IDCONTACT).size() != 0
				&& this.findContactByOneElementOption(String.valueOf(Cid),
						OPTION_FIND_BY_IDCONTACT).get(0) != null)
			return this.findContactByOneElementOption(String.valueOf(Cid),
					OPTION_FIND_BY_IDCONTACT).get(0);
		else
			return new Contact();
	}

	/**
	 * Find the contact By the name
	 * 
	 * @param name
	 * @return
	 */
	public List<Contact> findContactByName(String name) {
		return this.findContactByOneElementOption(name, OPTION_FIND_BY_NAME);
	}

	/**
	 * Find the contact By the firstame
	 * 
	 * @param firstName
	 * @return
	 */
	public List<Contact> findContactByFirstName(String firstName) {
		return this.findContactByOneElementOption(firstName,
				OPTION_FIND_BY_FIRSTNAME);
	}

	/**
	 * Find the contact By the Address
	 * 
	 * @param address
	 * @return
	 */
	public List<Contact> findContactByAddress(String address) {
		return this.findContactByOneElementOption(address,
				OPTION_FIND_BY_ADRESS);
	}

	/**
	 * Find the contact By the email
	 * 
	 * @param email
	 * @return
	 */
	public List<Contact> findContactByEmail(String email) {
		return this.findContactByOneElementOption(email, OPTION_FIND_BY_EMAIL);
	}

	/**
	 * Find the contact By the DayOfBirth
	 * 
	 * @param dateOfBirth
	 * @return
	 */
	public List<Contact> findContactByDateNaissance(Date dateOfBirth) {
		return this.findContactByOneElementOption(dateOfBirth.toString(),
				OPTION_FIND_BY_DATEOFBIRTH);
	}

	/***
	 * Add a new contact
	 * 
	 * @param newContact
	 * @return
	 */
	public boolean addContact(Contact newContact) {
		boolean added = false;
		newContact.setIdContact(contactList.size() + 1);
		contactList.add(newContact);
		return added;
	}

	/**
	 * Delete a contact
	 * 
	 * @param contact
	 * @return
	 */
	public boolean deleteContact(Contact contact) {
		boolean deleted = false;
		contactList.remove(contact);
		return deleted;
	}

	/**
	 * Modify the contact in the list
	 * 
	 * @param contact
	 * @param id
	 * @return
	 */
	public boolean modifyContact(Contact contact, int id) {
		boolean update = false;
		contactList.get(id);
		contactList.set(id, contact);
		return update;
	}

	public List<Contact> getAllContact() {
		return contactList;
	}

	public static ContactDTO getContactDTO() {
		if (contactDTO == null) {
			contactDTO = new ContactDTO();
		}
		return contactDTO;
	}

}
